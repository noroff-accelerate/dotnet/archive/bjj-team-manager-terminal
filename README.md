# BJJ Team Manager Terminal

.NET Core Console Application

Intended for learning Entity Framework as part of module 2 of Noroff Accelerate .NET Fullstack short course.

Demonstrates the use CRUD operations with a ono to one relationship


## Getting Started

Clone to a local directory.

Open solution in Visual Studio

Update the connection string to your local instance (SQLServer)

Run the seed methods (optional)

The call the CRUD methods to execute operations to the database

Run


### Prerequisites

.NET Framework

Visual Studio 2017/19 OR Visual Studio Code


## Authors

***Dean von Schoultz** [deanvons](https://gitlab.com/deanvons)





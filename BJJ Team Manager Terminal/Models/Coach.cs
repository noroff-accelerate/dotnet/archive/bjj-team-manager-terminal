﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BJJ_Team_Manager_Terminal.Models
{
    public class Coach
    {
        //This is the prinicple entity in the one to one relationship between Coach and CoachResume because the navigation propery (FK - Resume ID) is not specified
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        //the nullability of the property will determine the optionality of this relationship
        public CoachResume Resume { get; set; }


    }
}

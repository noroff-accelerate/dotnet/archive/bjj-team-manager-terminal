﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BJJ_Team_Manager_Terminal.Models
{
    public class CoachResume
    {
        //This is the dependant entity in the one to one relationship between Coach and CoachResume because the navigation propery is specified here
        public int Id { get; set; }
        public string Biography { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Nationality { get; set; }
        
        //the nullability of the navigation property will determine the optionality of this relationship
        public int CoachId { get; set; }
        public Coach Coach { get; set; }


    }
}

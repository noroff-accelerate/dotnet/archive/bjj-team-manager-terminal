﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace BJJ_Team_Manager_Terminal.Models
{
    public class BJJTeamDBContext:DbContext
    {
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<CoachResume> CoachResumes { get; set; }
    
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=DESKTOP-4J3HI0S\\SQLEXPRESS01;Database=BJJTeamTerminalDB;trusted_connection=true;MultipleActiveResultSets=true");
        }

    }

}

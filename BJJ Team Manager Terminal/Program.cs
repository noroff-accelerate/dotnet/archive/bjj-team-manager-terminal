﻿using System;
using System.Linq;
using BJJ_Team_Manager_Terminal.Models;
using Microsoft.EntityFrameworkCore;


namespace BJJ_Team_Manager_Terminal
{
    class Program
    {
        static void Main(string[] args)
        {
            
            DisplayCoaches();

        }

        #region Create
        public static void SeedCoachData()
        {

            //Create some coaches
            Coach coach1 = new Coach() { FirstName = "Pedro", LastName = "Sauer"};
            Coach coach2 = new Coach() { FirstName = "Roger", LastName = "Gracie" };
            Coach coach3 = new Coach() { FirstName = "Kit", LastName = "Dale" };

            //Add them to the database
            using (var context = new BJJTeamDBContext())
            {
                context.Coaches.Add(coach1);
                context.Coaches.Add(coach2);
                context.Coaches.Add(coach3);

                context.SaveChanges();
            }


            //Alternative syntax - Add anonymous objects to the database
            using (var context = new BJJTeamDBContext())
            {
                context.Coaches.Add(new Coach() { FirstName = "Marcelo", LastName = "Garcia" });
                context.Coaches.Add(new Coach() { FirstName = "Keenan", LastName = "Cornelius" });
                context.Coaches.Add(new Coach() { FirstName = "Dean", LastName = "Lister" });

                context.SaveChanges();
            }

        }
    
        //Adds one sample coach resume to the database assigned to the Coach Pedro Sauer seeded from SeedCoachData
        public static void SeedCoachResume()
        {

            using (var context = new BJJTeamDBContext())
            {

                int coachID = 0;
                Coach currentCoach;

                coachID = context.Coaches.Where(c => c.FirstName == "Pedro").FirstOrDefault<Coach>().Id;

                currentCoach = context.Coaches.Find(coachID);

                CoachResume currentCoachResume = new CoachResume { CoachId = coachID, Coach = context.Coaches.Find(coachID), Nationality = "Brazilian", PlaceOfBirth = "Rio De Jenero", Biography = "Pedro was one of the original founders of BJJ....", DateOfBirth = new DateTime(1978, 1, 18) };

                context.CoachResumes.Add(currentCoachResume);

                context.SaveChanges();
            }


        }

        public static void AddCoach(Coach coach)
        {
            using (var context = new BJJTeamDBContext())
            {
                context.Coaches.Add(coach);
                context.SaveChanges();
            }
        }

        public static void AddCoachResume(CoachResume coachResume, Coach coach)
        {
            if (coachResume.Coach == null)
            {
                coachResume.CoachId = coach.Id;

                coachResume.Coach = coach;
            }

            using (var context = new BJJTeamDBContext())
            {
                context.CoachResumes.Add(coachResume);
                context.SaveChanges();
            }

        }

        #endregion

        #region Read
        public static void DisplayCoaches()
        {
            using (var context = new BJJTeamDBContext())
            {

                foreach (var coach in context.Coaches.Include(c => c.Resume))
                {
                   
                    Console.WriteLine($"{coach.FirstName} {coach.LastName}");

                    if (coach.Resume != null)
                    {
                        Console.WriteLine($"Some more info on this coach: He was born on {coach.Resume.DateOfBirth} and {coach.Resume.Biography}");

                    }
                }
            }

        }
        #endregion

        #region Update
        public static void UpdateBiography(string newBioInfo, int coachID, bool rewrite)
        {
            using (var context = new BJJTeamDBContext())
            {
                foreach (var coach in context.Coaches.Include(c => c.Resume))
                {
                   if(coach.Id == coachID)
                    {
                        if (rewrite != true)
                        {
                            coach.Resume.Biography += $" {newBioInfo}";
                        }
                        else
                        {
                            coach.Resume.Biography = $" {newBioInfo}";
                        }
                    }
                }
                context.SaveChanges();
            }
        }

        #endregion

        #region Delete
        public static void RemoveCoach(int coachID)
        {
            using (var context = new BJJTeamDBContext())
            {
                foreach (var coach in context.Coaches.Include(c => c.Resume))
                {
                    if (coach.Id == coachID)
                    {
                        
                        context.Coaches.Remove(coach);

                        //Because CoachResume is the dependant in the one to one relationship if we delete a coach with a resume the coachresume entry will also be deleted we do not have to explicitly run the commented out statement below (this is called 'cascade')
                        //context.CoachResumes.Remove(context.CoachResumes.Find(coach.Resume.Id));

                        break;
                    }
                }
                context.SaveChanges();
            }




        }
        #endregion
    
    
    
    
    
    
    }
}
